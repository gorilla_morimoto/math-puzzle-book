package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {
	ans := make([]int, 0, 5)
	i := 13
	x := 0

	for len(ans) < 5 {
		x = fib(i)
		if x%sumOfDigits(x) == 0 {
			ans = append(ans, x)
		}
		i++
	}

	fmt.Println(ans)
}

func fib(n int) int {
	dp := make([]int, n+1)
	dp[0] = 0
	dp[1] = 1

	for i := 2; i <= n; i++ {
		dp[i] = dp[i-2] + dp[i-1]
	}

	return dp[n]
}

var (
	m = make(map[int]int)
)

func fibRec(n int) int {
	if n == 0 {
		return 0
	} else if n == 1 {
		return 1
	}

	x, ok := m[n]
	if ok {
		return x
	}

	m[n] = fibRec(n-2) + fibRec(n-1)
	return m[n]
}

func sumOfDigits(x int) int {
	s := strings.Split(strconv.Itoa(x), "")
	sum := 0

	for _, v := range s {
		n, _ := strconv.Atoi(v)
		sum += n
	}

	return sum
}
