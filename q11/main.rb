def fib(n)
  dp = []
  dp[0] = 0
  dp[1] = 1

  2.upto(n) do |i|
    dp[i] = dp[i - 1] + dp[i - 2]
  end

  dp[n]
end

@m = { 1 => 1, 2 => 1 }

def fib_rec(n)
  return @m[n] unless @m[n].nil?

  @m[n] = fib_rec(n - 1) + fib_rec(n - 2)
end

ans = []
i = 13
loop do
  x = fib(i)
  ans.push x if x % x.to_s.split('').map(&:to_i).inject(:+) == 0
  break if ans.size == 5
  i += 1
end

p ans
