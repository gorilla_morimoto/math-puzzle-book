def palindrome?(n)
  n.to_s == n.to_s.reverse &&
    n.to_s(2) == n.to_s(2).reverse &&
    n.to_s(8) == n.to_s(8).reverse
end

n = 11

loop do
  break if palindrome?(n)
  n += 2
end

puts n
