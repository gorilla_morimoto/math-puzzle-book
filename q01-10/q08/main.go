package main

import "fmt"

func main() {
	fmt.Println(move([][2]int{{0, 0}}))
}

func move(log [][2]int) int {
	if len(log) == 13 {
		return 1
	}

	count := 0
	next := [][2]int{{1, 0}, {0, 1}, {-1, 0}, {0, -1}}
	for _, v := range next {
		nextPosition := [2]int{
			log[len(log)-1][0] + v[0],
			log[len(log)-1][1] + v[1],
		}

		if include(nextPosition, log) {
			continue
		}

		// log = append(log, nextPosition)ではだめ
		// appendはスライスに余裕がある場合はリスライスで済ませるため

		//newLog := append(log, nextPosition)
		// このコードでもなぜかうまくいくのだが、理由がわからない

		// copy()は要素が少ない方に合わせてコピーする量を調整する
		// （どちらかが短ければ、それに合わせて残りはコピーされない）
		// ので、makeはlen(log)でする必要がある
		newLog := make([][2]int, len(log))
		copy(newLog, log)
		newLog = append(newLog, nextPosition)

		count += move(newLog)
	}

	return count
}

func include(x [2]int, ls [][2]int) bool {
	if len(ls) == 0 {
		return false
	}

	if x == ls[0] {
		return true
	}

	return include(x, ls[1:])
}
