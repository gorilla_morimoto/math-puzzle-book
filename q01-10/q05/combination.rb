coins = [10, 50, 100, 500]
count = 0

# Array.repeated_combination は「順序なし、重複を許す」組み合わせを作る
# 重複を許さない組み合わせだと Array.combination を使う
(2..15).each do |i|
  coins.repeated_combination(i).each do |coin_set|
    count += 1 if coin_set.inject(:+) == 1000
  end
end

puts count
