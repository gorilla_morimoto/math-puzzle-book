package main

import "fmt"

var (
	count int
)

func main() {
	change(1000, []int{500, 100, 50, 10}, 15)
	fmt.Println(count)
}

func change(target int, coins []int, usable int) {
	coin := coins[0]
	coins = coins[1:]

	if len(coins) == 0 {
		if target/coin <= usable {
			count++
		}
	} else {
		for i := 0; i < target/coin+1; i++ {
			// copyするときにはコピー先が初期化されていないとだめっぽい（要素を入れておく必要がある）
			// https://stackoverflow.com/questions/30182538/why-can-not-i-duplicate-a-slice-with-copy-in-golang
			coppiedCoins := make([]int, len(coins))
			copy(coppiedCoins, coins)
			change(target-coin*i, coppiedCoins, usable-i)
		}
	}

}
