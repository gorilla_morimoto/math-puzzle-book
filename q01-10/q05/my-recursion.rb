# 2018-03-12
# 値を返す関数にしたかったのですがだめでした……
# 精進しましょう

def change(rest, coins, howmany_coins)
  if rest < 0 
    0
  elsif rest.zero?
    1
  else
    return 0 if coins.empty?
    next_coin = coins.shift
    usable_coins = rest / next_coin
    0.upto(usable_coins) do |i|
      change(rest - next_coin * i, coins.clone, howmany_coins - i)
    end
  end
end

puts change(1000, [500, 100, 50, 10], 15)
