n = 100
cards = Array.new(n, false)

2.upto(cards.size) do |i|
  j = i - 1
  while j < cards.size
    cards[j] = !cards[j]
    j += i
  end
end

n.times do |i|
  puts i + 1 unless cards[i]
end
