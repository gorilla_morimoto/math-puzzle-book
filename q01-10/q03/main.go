package main

import "fmt"

func main() {
	n := 100

	m := make(map[int]bool)
	for i := 2; i <= n; i++ {
		for j := i; j <= n; j += i {
			m[j] = !m[j]
		}
	}

	for i := 1; i <= n; i++ {
		if !m[i] {
			fmt.Println(i)
		}
	}
}
