# currentは現在の本数
def cutbar(m, n, current)
  return 0 if current >= n

  if current < m
    1 + cutbar(m, n, current * 2)
  else
    1 + cutbar(m, n, current + m)
  end
end

puts cutbar(3, 20, 1)
puts cutbar(5, 100, 1)
