package main

import (
	"fmt"
)

func main() {
	fmt.Println(cutbar(3, 20, 1))
	fmt.Println(cutbar(5, 100, 1))

}

func cutbar(m, n, current int) int {
	if current >= n {
		return 0
	} else if current <= m {
		return 1 + cutbar(m, n, current*2)
	} else {
		return 1 + cutbar(m, n, current+m)
	}
}
