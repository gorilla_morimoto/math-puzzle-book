def cutbar(m,n)
  current = 1
  i = 0
  while current < n
    current += current < m ? current : m
    i += 1
  end
  i
end

puts cutbar(3, 20)
puts cutbar(5, 100)
