package main

import "fmt"

func collatzKai(n, m int) int {
	if m == 1 {
		return 0
	} else if m == n {
		return 1
	}

	if m%2 == 0 {
		return collatzKai(n, m/2)
	}
	return collatzKai(n, m*3+1)
}

func main() {
	count := 0
	for i := 2; i <= 10000; i++ {
		count += collatzKai(i, i*3+1)
	}
	fmt.Println(count)
}
