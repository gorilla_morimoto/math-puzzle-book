package main

import (
	"bytes"
	"fmt"
	"math"
)

func main() {
	i := 1
	for {
		i++
		if f(float64(i)) {
			break
		}
	}
	fmt.Println(i)
}

func f(x float64) bool {
	s := fmt.Sprintf("%.50f", math.Sqrt(x))
	if len(uniquen(s)) == 10 {
		return true
	}
	return false
}

func uniquen(s string) string {
	m := make(map[rune]bool)
	buf := bytes.NewBufferString("")

	for _, v := range s {
		_, ok := m[v]
		if ok {
			continue
		}
		m[v] = true
		buf.WriteRune(v)
	}

	return buf.String()
}
