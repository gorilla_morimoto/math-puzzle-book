n = 10
@steps = 4

def move(a, b)
  return 0 if a > b
  return 1 if a == b

  count = 0
  (1..@steps).each do |da|
    (1..@steps).each do |db|
      count += move(a + da, b - db)
    end
  end

  count
end

puts move(0, n)
