n = 10
@steps = 4

dp = Array.new(n + 1, 0)
count = 0
dp[n] = 1

n.times do |i|
  (n + 1).times do |j|
    (1..@steps).each do |k|
      break if k > j
      dp[j - k] += dp[j]
    end
    dp[j] = 0
  end
  count += dp[0] if i.odd?
end

puts count
