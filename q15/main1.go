package main

import "fmt"

var (
	n     = 10
	steps = 4
)

func main() {
	fmt.Println(move(0, n))
}

func move(a int, b int) int {
	if a == b {
		return 1
	}
	if a > b {
		return 0
	}

	count := 0
	for i := 1; i <= steps; i++ {
		for j := 1; j <= steps; j++ {
			count += move(a+i, b-j)
		}
	}

	return count
}
