package main

import "fmt"

var (
	n     = 10
	steps = 4
	m     = make(map[[2]int]int)
)

func main() {
	fmt.Println(move(0, n))

}

func move(a int, b int) int {
	if a == b {
		return 1
	}
	if a > b {
		return 0
	}
	x, ok := m[[2]int{a, b}]
	if ok {
		return x
	}

	count := 0
	for da := 1; da <= steps; da++ {
		for db := 1; db <= steps; db++ {
			count += move(a+da, b-db)
		}
	}

	m[[2]int{a, b}] = count
	return count
}
